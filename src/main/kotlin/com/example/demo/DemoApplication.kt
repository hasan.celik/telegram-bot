package com.example.demo

import com.github.kotlintelegrambot.Bot
import com.github.kotlintelegrambot.bot
import com.github.kotlintelegrambot.dispatch
import com.github.kotlintelegrambot.dispatcher.command
import com.github.kotlintelegrambot.entities.ChatId
import com.github.kotlintelegrambot.webhook
import it.skrape.core.htmlDocument
import it.skrape.fetcher.AsyncFetcher
import it.skrape.fetcher.extractIt
import it.skrape.fetcher.skrape
import it.skrape.selects.html5.div
import it.skrape.selects.html5.h3
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.boot.runApplication
import org.springframework.context.event.EventListener
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@SpringBootApplication
class DemoApplication

data class Food(
    val foodText: String
)

data class Meal(
    val mealType: String,
    val foods: List<Food>
)

data class Outlet(
    val name: String,
    val meals: List<Meal>
)

data class MensaData(
    var place: String = "",
    var outlets: List<Outlet> = listOf()
)

@Service
class MensaService {
    private var logger = LoggerFactory.getLogger(javaClass)

    fun init() {
        runBlocking {
            launch {
                logger.info(getMensa().toString())
            }
        }
    }

    suspend fun getMensa(): MensaData {
        logger.info("Starting skrape")

        return skrape(AsyncFetcher) {
            request {
                url = "https://www.studentenwerk-oldenburg.de/de/gastronomie/speiseplaene/speiseplan-fuer-heute.html"
            }
            extractIt<MensaData> {
                htmlDocument {
                    it.place = div("#speiseplan") { findFirst { h3 { findFirst { text } } } }
                }
            }
        }
    }
}

@Service
class BotService(val mensaService: MensaService) {

    private var logger = LoggerFactory.getLogger(javaClass)

    @Value("\${token}")
    private lateinit var botToken: String

    @Value("\${host}")
    private lateinit var host: String

    private lateinit var bot: Bot

    @EventListener(ApplicationReadyEvent::class)
    fun init() {
        bot = bot {
            token = botToken
            webhook {
                url = "${host}/${botToken}"
            }
            dispatch {
                command("hello") {
                    bot.sendMessage(ChatId.fromId(message.chat.id), "World")
                }
                command("mensa") {
                    logger.info("Starting")
                    runBlocking {
                        launch {
                            val mensaData = mensaService.getMensa()
                            logger.info(mensaData.toString())
                            bot.sendMessage(ChatId.fromId(message.chat.id), mensaData.toString())
                        }
                    }
                }
            }
        }
        logger.info("Webhook started: ${bot.startWebhook()}")
    }

    fun update(update: String) {
        bot.processUpdate(update)
    }
}

@RequestMapping
@RestController
class DefaultController(private val botService: BotService) {

    @PostMapping("/\${token}")
    fun update(@RequestBody update: String): ResponseEntity<String> {
        botService.update(update)
        return ResponseEntity.ok(update)
    }
}

fun main(args: Array<String>) {
    runApplication<DemoApplication>(*args)
}

